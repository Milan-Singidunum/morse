package rs.ac.singidunum.fir.testiranje_softvera.morse_code;

/**
 * An enum type whose values represent a dot, dash and a pause used in Morse code to represent symbols.
 */
enum Signal {
    /**
     * The enum value representing a dot (single unit of duration)
     */
    DOT,

    /**
     * The enum value representing a dash (three units of duration)
     */
    DASH,

    /**
     * The enum value representing a pause between letters (three units of duration)
     */
    PAUSE;

    /**
     * Returns the appropriate Signal value depending on the given character.
     * 1) For characters . (full stop) and * (asterisk) it returns DOT.
     * 2) For characters - (dash) and _ (underscore) it returns DASH.
     * 3) For a space character it returns PAUSE.
     * @param character The character to decode
     * @return The value of the enum type depending on the given character or null if unable to decode.
     */
    public static Signal decodeCharacter(char character) {
        switch (character) {
            case '.' : return DOT;
            case '*' : return DOT;
            case '-' : return DASH;
            case '_' : return DASH;
            case ' ' : return PAUSE;
            default  : return null;
        }
    }
}
