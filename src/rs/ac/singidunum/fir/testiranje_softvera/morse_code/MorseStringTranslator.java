package rs.ac.singidunum.fir.testiranje_softvera.morse_code;

/**
 * This class provides the user with a single static method that translates Morse code stored as a string
 * into a string of symbols the given Morse code represents.
 */
public class MorseStringTranslator {
    /**
     * This method translates Morse code stored as a string into a string of symbols the given Morse code represents.
     * @param code The string holding the Morse code message.
     *             Dots are represented by either a . (full stop) or an * (asterisk).
     *             Dashes are represented by either a - (dash) or an _ (underscore).
     *             Pauses are represented by a single space character.
     * @return The decoded string of symbols. The string contains only supported decoded symbols.
     *         Unsupported signal sequences are skipped in decoding.
     */
    public static String translate(String code) {
        final SignalTranslator signalTranslator = new SignalTranslator();
        final StringBuilder translation = new StringBuilder();

        code += " ";

        for (char character : code.toCharArray()) {
            Signal signal = Signal.decodeCharacter(character);

            signalTranslator.receiveSignal(signal);

            if (signalTranslator.isSymbolIdentified()) {
                translation.append(signalTranslator.getIdentifiedSymbol());
            }
        }

        return translation.toString();
    }
}
