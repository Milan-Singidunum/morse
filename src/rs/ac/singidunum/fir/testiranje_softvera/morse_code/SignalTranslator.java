package rs.ac.singidunum.fir.testiranje_softvera.morse_code;

import java.util.*;

import static rs.ac.singidunum.fir.testiranje_softvera.morse_code.Signal.DOT;
import static rs.ac.singidunum.fir.testiranje_softvera.morse_code.Signal.DASH;
import static rs.ac.singidunum.fir.testiranje_softvera.morse_code.Signal.PAUSE;

/**
 * This class handles translation of sequences of dots, dashes and pauses fed to it into symbols according to Morse code.
 */
public class SignalTranslator {
    /**
     * This list is used to hold the sequence of dots, dashes and pauses fed to the class using the receiveSignal method.
     */
    final private List<Signal> sequence = new LinkedList<>();

    /**
     * This variable is used to hold and identified symbol when one is found, based on the currently standing sequence of signals.
     */
    private Character identifiedSymbol = null;

    /**
     * This structure holds all supported combinations of signal sequences and their respective symbols.
     * It is used to lookup the currently stored sequence of signals and decode it into a symbol.
     */
    private Map<Signal[], Character> dictionary = initDictionary();

    /**
     * This method initialises the map that holds all supported combinations of signal sequences and their respective symbols.
     */
    private Map<Signal[], Character> initDictionary() {
        Map<Signal[], Character> dictionary = new HashMap<>();

        // Letters
        dictionary.put(new Signal[]{DOT, DASH}, 'A');
        dictionary.put(new Signal[]{DASH, DOT, DOT, DOT}, 'B');
        dictionary.put(new Signal[]{DASH, DOT, DASH, DOT}, 'C');
        dictionary.put(new Signal[]{DASH, DOT, DOT}, 'D');
        dictionary.put(new Signal[]{DOT}, 'E');
        dictionary.put(new Signal[]{DOT, DOT, DASH, DOT}, 'F');
        dictionary.put(new Signal[]{DASH, DASH, DOT}, 'G');
        dictionary.put(new Signal[]{DOT, DOT, DOT, DOT, DOT}, 'H');
        dictionary.put(new Signal[]{DOT, DOT}, 'I');
        dictionary.put(new Signal[]{DOT, DASH, DASH, DASH}, 'J');
        dictionary.put(new Signal[]{DASH, DOT, DASH}, 'K');
        dictionary.put(new Signal[]{DOT, DASH, DOT, DOT}, 'L');
        dictionary.put(new Signal[]{DASH, DASH}, 'M');
        dictionary.put(new Signal[]{DASH, DOT}, 'N');
        dictionary.put(new Signal[]{DASH, DASH, DASH}, 'O');
        dictionary.put(new Signal[]{DOT, DASH, DASH, DOT}, 'P');
        dictionary.put(new Signal[]{DASH, DASH, DOT, DASH}, 'Q');
        dictionary.put(new Signal[]{DOT, DASH, DOT}, 'R');
        dictionary.put(new Signal[]{DOT, DOT, DOT}, 'S');
        dictionary.put(new Signal[]{DASH}, 'T');
        dictionary.put(new Signal[]{DOT, DOT, DASH}, 'U');
        dictionary.put(new Signal[]{DOT, DOT, DOT, DASH}, 'V');
        dictionary.put(new Signal[]{DOT, DASH, DASH}, 'W');
        dictionary.put(new Signal[]{DASH, DOT, DOT, DASH}, 'X');
        dictionary.put(new Signal[]{DASH, DOT, DASH, DASH}, 'Y');
        dictionary.put(new Signal[]{DASH, DASH, DOT, DOT}, 'Z');

        // Digits
        dictionary.put(new Signal[]{DOT, DASH, DASH, DASH, DASH}, '1');
        dictionary.put(new Signal[]{DOT, DOT, DASH, DASH, DASH}, '2');
        dictionary.put(new Signal[]{DOT, DOT, DOT, DASH, DASH}, '3');
        dictionary.put(new Signal[]{DOT, DOT, DOT, DOT, DASH}, '4');
        dictionary.put(new Signal[]{DOT, DOT, DOT, DOT, DOT}, '5');
        dictionary.put(new Signal[]{DASH, DOT, DOT, DOT, DOT}, '6');
        dictionary.put(new Signal[]{DASH, DASH, DOT, DOT, DOT}, '7');
        dictionary.put(new Signal[]{DASH, DASH, DASH, DOT, DOT}, '8');
        dictionary.put(new Signal[]{DASH, DASH, DASH, DASH, DOT}, '9');
        dictionary.put(new Signal[]{DASH, DASH, DASH, DASH, DASH}, '0');

        // Interpunction
        dictionary.put(new Signal[]{DOT, DOT, DOT, DOT, DOT, DOT}, ' ');

        return dictionary;
    }

    /**
     * This method receives the value of the signal. If the signal is null, it is ignored.
     * If it is a DOT or a DASH, the method stores it into the current sequence for decoding.
     * If it is a PAUSE signal, then the sequence is evaluated in an attempt to identify the symbol.
     * When a supported symbol is identified by the currently stored sequence it is stored into the identifiedSymbol\
     * variable and the current sequence list is cleared, pending future signal reception.
     * @param signal The value of the signal being received and processed in the sequence
     */
    public void receiveSignal(Signal signal) {
        if (signal == null) {
            return;
        }

        identifiedSymbol = null;

        if (signal == PAUSE) {
            findSymbol();
            sequence.clear();
            return;
        }

        sequence.add(signal);
    }

    /**
     * This method performs logic that searches for a sequence in the dictionary that matches to the sequence
     * currently held in the signal sequence list. If a symbol is found, it stores it into the identifiedSymbol variable.
     * Otherwise, the identifiedSymbol variable remains null.
     */
    private void findSymbol() {
        identifiedSymbol = null;

        dictionary.keySet()
            .stream()
            .filter(map -> Arrays.equals(sequence.toArray(), map))
            .forEach(map -> identifiedSymbol = dictionary.get(map));
    }

    /**
     * Returns true if the identified symbol is no longer null. Otherwise it is False.
     * @return True if the identified symbol is no longer null. Otherwise it is False.
     */
    public boolean isSymbolIdentified() {
        return identifiedSymbol != null;
    }

    /**
     * Returns the value of the identified symbol, based on the current sequence.
     * @return The value of the identified symbol, based on the current sequence.
     */
    public Character getIdentifiedSymbol() {
        return identifiedSymbol;
    }
}
